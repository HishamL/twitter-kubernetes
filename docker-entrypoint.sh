#!/bin/sh

set -e

rm -f /alitwitter/tmp/pids/server.pid

exec "$@"
