# Alitwitter

## Description

A simple twitter app that support CRUD operation for tweet only.

## Dependencies

- Docker
  You must install docker first from this docker [doc](https://docs.docker.com/install/) to install it in your machine before you can build the docker image
- Kubernetes (kubectl)
  You can install kubectl to access kubernetes command line interface from this [doc](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- Minikube
  You can install minikube from [here](https://kubernetes.io/docs/tasks/tools/install-minikube/)

## Environment Setup

- Ruby 2.6.5
- Rails 6.0.2.1
- Bundler 2.1.4

After installing ruby, then you must first install rails with this
```
gem install rails -v 6.0.2.1
```

Then, install all depedency using bundle in root of directory project.
```
bundle install
```

## Run Test

Using rspec & rubocop
```
bundle exec rake
```

To open the coverage result in browser
```
bundle exec rake coverage
```

## How to Run

### Docker

You can build the image of the app first with
```
docker build -t . alitwitter
```

and you can run with
```
docker run -p 3000:3000 -e RAILS_ENV=<rails-env> -e SECRET_KEY_BASE=<secret> -e DB_PRODUCTION=<db-name> alitwitter
```

### Kubernetes

You can also deploy the image you build from docker using kubernetes with the following command
```
# Apply configmap and secret first
kubectl apply -f kubernetes/configmap.yml
kubectl apply -f kubernetes/secret.yml

# Apply deployment
kubectl apply -f kubernetes/deployment.yml

# Apply service to expose pods
kubectl apply -f kubernetes/service.yml
```

And you can access the application using
```
minikube service alitwitter-service
```

Or directly using you `<minikube-ip>:<node-port>`

### Local

To run this application, you must run this first
```
yarn

rake db:migrate
```

and then run the rails app with
```
rails s
```

It will open the default twitter page directly.
