FROM ruby:2.6.5-alpine3.11 as builder

RUN apk add --update --no-cache \
    build-base \
    postgresql-dev \
    sqlite-dev \
    nodejs \
    yarn \
    tzdata

WORKDIR /alitwitter

COPY Gemfile* ./

RUN gem install -v '2.1.4' bundler

RUN bundle config --global frozen 1 \
 && bundle install --without development test -j4 \
 # Remove unneeded files (cached *.gem, *.o, *.c)
 && rm -rf /usr/local/bundle/cache/*.gem \
 && find /usr/local/bundle/gems/ -name "*.c" -delete \
 && find /usr/local/bundle/gems/ -name "*.o" -delete

# Install yarn packages
COPY package.json yarn.lock ./
RUN yarn install

# Copy the Rails app
COPY . ./

# Precompile assets
RUN RAILS_ENV=production SECRET_KEY_BASE=just_some_secret_key bundle exec rake assets:precompile

# Remove folders not needed in resulting image
RUN rm -rf node_modules tmp/cache vendor/assets lib/assets spec

# Final stage
FROM ruby:2.6.5-alpine3.11

RUN apk add --update --no-cache \
    postgresql-client \
    sqlite \
    sqlite-dev \
    tzdata

RUN gem install bundler -v '2.1.4'

# Add user
RUN addgroup -g 1000 -S app \
    && adduser -u 1000 -S app -G app
USER app

# Copy app with gems from former build stage
COPY --from=builder /usr/local/bundle/ /usr/local/bundle/
COPY --from=builder --chown=app:app /alitwitter /alitwitter

# Set Rails env
ENV RAILS_LOG_TO_STDOUT true
ENV RAILS_SERVE_STATIC_FILES true
ENV EXECJS_RUNTIME Disabled

COPY docker-entrypoint.sh /usr/bin
USER root
RUN chmod +x /usr/bin/docker-entrypoint.sh
USER app
ENTRYPOINT [ "docker-entrypoint.sh" ]

WORKDIR /alitwitter

# Expose Puma port
EXPOSE 3000

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
