require 'rails_helper'

RSpec.describe TweetsController, type: :controller do
  describe '#index' do
    it 'should return success response' do
      get :index
      expect(response).to be_successful
    end

    it 'renders the index template' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe '#create' do
    context 'given valid params to create a tweet' do
      it 'should redirect to index path' do
        post :create, params: { tweet: { content: 'some random tweet' } }
        expect(response).to redirect_to(tweets_path)
      end
    end

    context 'given invalid params to create a tweet' do
      it 'should redirect to index path' do
        long_content = 'This is some content that exceeds the 140 string length' \
                        'which counts as invalid tweet and will not be saved to database'\
                        'in which the string is so long that even the real twitter can\'t handle'
        post :create, params: { tweet: { content: long_content } }
        expect(response).to redirect_to(tweets_path)
      end
    end
  end

  describe '#destroy' do
    before do
      @tweet = Tweet.create(content: 'some random content')
    end

    subject { delete :destroy, params: { id: @tweet.id } }

    context 'given specific tweet to delete' do
      it 'should delete that specific tweet' do
        expect { subject }.to change(Tweet, :count).by(-1)
      end
    end

    context 'given specific tweet to delete' do
      it 'should redirect to index path' do
        subject
        expect(response).to redirect_to(tweets_path)
      end
    end
  end
end
