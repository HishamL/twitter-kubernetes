class TweetsController < ApplicationController
  def index
    @tweets = Tweet.all.order('created_at DESC')
  end

  def create
    @tweet = Tweet.create(params.require(:tweet).permit(:content))
    flash[:errors] = @tweet.errors.full_messages unless @tweet.valid?
    redirect_to tweets_path
  end

  def destroy
    @tweet = Tweet.find(params[:id])
    @tweet.destroy
    redirect_to tweets_path
  end
end
